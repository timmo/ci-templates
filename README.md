# CI Templates

[![pipeline status](https://gitlab.com/timmo/ci-templates/badges/master/pipeline.svg)](https://gitlab.com/timmo/ci-templates/commits/master)

GitLab CI templates for use with `.gitlab-ci.yml` files.

## Usage

```yaml
include: 'https://gitlab.com/timmo/ci-templates/raw/master/preflight/yamllint.yml'
```
